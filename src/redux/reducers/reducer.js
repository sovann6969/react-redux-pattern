import {actionType} from '../action/actionType'
// This is the default state of the app i.e. when the app starts for the first time
const initialState = {
  counter: 0
}

// This is a reducer which listens to actions and modifies the state
export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.ADD_TO_COUNTER:{
        return {
            ...state,
            counter: state.counter + 1
          }
    }
     
    default:
      return state
    }
}