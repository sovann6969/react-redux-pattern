import {actionType} from './actionType'
export function addToCounter() {
    return {
      type: actionType.ADD_TO_COUNTER
    }
  }