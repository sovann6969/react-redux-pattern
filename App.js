import React, { Component } from 'react'
import { Text, Button, View } from 'react-native'
import { connect } from 'react-redux'
import {addToCounter} from '../Redux/src/redux/action/action'
// import moduleName from './src/redux/'
class App extends Component {
  render() {
    return (
      <View>
        {/* // this.props.count comes from the Redux state */}
        <Text>{this.props.count}</Text>

        {/* // This.props.addToCounter() is a function to update the counter */}
        <Button title=" Click Me!" onPress={() => this.props.addToCounter()}>
         
      </Button>
      </View>

    )
  }
}

// This function provides a means of sending actions so that data in the Redux store
// can be modified. In this example, calling this.props.addToCounter() will now dispatch
// (send) an action so that the reducer can update the Redux state.
function mapDispatchToProps(dispatch) {
  return {
    addToCounter: () => dispatch(addToCounter())
  }
}

// This function provides access to data in the Redux state in the React component
// In this example, the value of this.props.count will now always have the same value
// As the count value in the Redux state
function mapStateToProps(state) {
  console.log(state)
  return {
    count: state.counter
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)