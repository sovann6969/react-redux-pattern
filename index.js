/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import React,{Component} from 'react'
import {Provider} from 'react-redux'
import configureStore  from './src/redux/store/store'
const store = configureStore ()

class MyCounterApp extends Component {
  render() {
    return(
      <Provider store={store}>
        <App/>
      </Provider>
    )
  }
}

AppRegistry.registerComponent(appName, () => MyCounterApp);
